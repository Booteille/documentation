# Dealing with spammers

With open registrations on your server, chances are you will have to deal with spam.
There is no simple and definitive way to solve this problem while keeping registrations open,
but there are several that could help you limit spam and its impact on your instance.

## Enable videos quarantine/auto video blacklisting 

**Available in PeerTube >= 1.3**

You can enable this feature in *Administration* -> *Configuration*. 
When enabled, every new uploaded videos will be hidden by default until a moderator manually approves them.

You can then individually allow users you trust to bypass videos quarantine in *Administrations* -> *Users* -> *Update a user*.

## Disable new registrations

You can disable new registrations, and update your instance description to explain that you accept new users on demand.
They can for example use the contact button of the about page.

## Require email verification

Force email verification on signup in *Administration* -> *Configuration*.
Unfortunately this feature is not really useful since most spammers use a real email account (gmail etc) and automate the validation process.
